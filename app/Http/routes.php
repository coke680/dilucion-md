<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

//get json productos para diferentes vistas
Route::get('/getProducts', 'ProductController@getProducts');
Route::get('/products/getProductsDetail/{id}', 'ProductController@getProductsDetail');

Route::group(['middleware' => 'auth'], function(){

	Route::get('/home', 'HomeController@index');
	Route::get('/products', 'ProductController@index');

	//CRUD de productos
	Route::get('/products/create', 'ProductController@create');
	Route::post('/products', 'ProductController@store');
	Route::get('/products/del/{id}', ['uses' => 'ProductController@destroy']);
	Route::get('/products/edit/{id}', ['uses' => 'ProductController@edit']);
	Route::post('/products/{id}', ['uses' => 'ProductController@update']);

	Route::get('/dilutions', 'DilutionController@index');
	//CRUD de recomendaciones
	Route::get('/dilutions/create', 'DilutionController@create');
	Route::post('/dilutions', 'DilutionController@store');
	Route::get('/dilutions/del/{id}', ['uses' => 'DilutionController@destroy']);
	Route::get('/dilutions/edit/{id}', ['uses' => 'DilutionController@edit']);
	Route::post('/dilutions/{id}', ['uses' => 'DilutionController@update']);
});




